// FIXME
#![allow(unreachable_code)]

#[derive(Debug, PartialEq)]
enum TokenKind {
    Int(u64),
    Char(char),
    Float(f64),
    Str(String),
    Name(String),
    Keyword(String),

    EOF,
    Less,
    Greater,
    Not,
    AndBit,
    AndLogic,
    OrBit,
    OrLogic,
    Xor,
    Add,
    Subtract,
    Multiply,
    Divide,
    Modulus,
    Equal,
    LShift,
    RShift,
    NotEq,
    LtEq,
    GtEq,
    Assign,
    Declare,
    DeclareAssign,
    AddAssign,
    SubAssign,
    OrBitAssign,
    OrLogicAssign,
    AndLogicAssign,
    AndBitAssign,
    XorAssign,
    LShiftAssign,
    RShiftAssign,
    MulAssign,
    DivAssign,
    ModAssign,

    OpenParen,
    CloseParen,
    OpenCurly,
    CloseCurly,
    OpenSquareBracket,
    CloseSquareBracket,
    Comma,
    SemiColon,
}
use self::TokenKind::*;


#[derive(Debug, PartialEq)]
struct Token {
    kind: TokenKind,
    string: String,
}

impl Token {
    fn new(kind: TokenKind, string: &str) -> Token {
        Token { kind, string: String::from(string) }
    }
}

const KEY_WORDS: [&str; 10] = [
    "if",
    "else",
    "true",
    "false",
    "u8",
    "u16",
    "u32",
    "u64",
    "f32",
    "f64",
];


struct Stream {
    data: String,
    index: usize,
}

impl Stream {
    fn get_char_at_index(&self, index: usize) -> Option<char> {
        self.data.chars().nth(index)
    }

    fn get_char_at_index_or_panic(&self, index: usize) -> char {
        if let Some(ch) = self.get_char_at_index(index) {
            ch
        } else {
            panic!("Reached EOF");
        }
    }

    fn get_current_char(&self) -> Option<char> {
        self.data.chars().nth(self.index)
    }

    fn get_current_char_or_panic(&self) -> char {
        if let Some(ch) = self.get_current_char() {
            ch
        } else {
            panic!("Reached EOF");
        }
    }

    fn from(string: &str) -> Stream {
        Stream {
            data: String::from(string),
            index: 0
        }
    }

    fn scan_char(&mut self) -> Option<Token> {
        let token_string_start = self.index;
        self.index += 1;
        let val = match self.get_current_char() {
            None => panic!("Reached EOF in char literal"),
            Some('\'') => panic!("Char literals cannot be empty"),
            Some('\n') => panic!("Char literals cannot contain newline"),
            Some('\\') => {
                self.index += 1;
                match self.get_current_char() {
                    None => panic!("Reached EOF in char literal"),
                    Some('0') => '\0',
                    Some('n') => '\n',
                    Some('r') => '\r',
                    Some('t') => '\t',
                    Some(default) => panic!("Invalid character escape letter '\\{}'", default),
                }
            },
            Some(default) => default,
        };
        self.index += 1;
        if self.get_current_char() != Some('\'') {
            panic!("Expected closing char quote");
        }
        self.index += 1;

        let string = String::from(&self.data[token_string_start..self.index]);
        return Some(Token { kind: Char(val), string });
    }

    fn scan_keyword_or_name(&mut self) -> Option<Token> {
        let string_start = self.index;
        self.index += 1;
        while let Some(ch) = self.get_current_char() {
            if ch.is_alphanumeric() || ch == '_' {
                self.index += 1;
            } else {
                break;
            }
        }

        let string_end = self.index;
        let string = &self.data[string_start..string_end];

        if string.is_empty() {
            return None;
        }

        let kind = if KEY_WORDS.contains(&string) {
            Keyword(String::from(string))
        } else {
            Name(String::from(string))
        };

        let string = string.to_string();

        return Some(Token { kind, string });
    }

    fn scan_string(&mut self) -> Option<Token> {
        let token_string_start = self.index;
        self.index += 1;
        let string_start = self.index;
        loop {
            if let Some(ch) = self.get_current_char() {
                if ch == '"' { break; }
                self.index += 1;
            } else {
                panic!("Unclosed \" delimiter");
            }
        }
        let string_end = self.index;
        let string = String::from(&self.data[string_start..string_end]);

        self.index += 1;
        let token_string_end = self.index;
        let token_string = String::from(&self.data[token_string_start..token_string_end]);

        return Some(Token { kind: Str(string), string: token_string });
    }

    fn scan_float(&mut self) -> Option<Token> {
        // FIXME: remove panics
        let token_string_start = self.index;
        while self.get_current_char_or_panic().is_digit(10) {
            self.index += 1;
        }
        if self.get_current_char_or_panic() == '.' {
            self.index += 1;
        }
        while self.get_current_char_or_panic().is_digit(10) {
            self.index += 1;
        }
        if self.get_current_char_or_panic().to_ascii_lowercase() == 'e' {
            self.index += 1;
            match self.get_current_char_or_panic().to_ascii_lowercase() {
                '+' | '-' => self.index += 1,
                _ => (),
            }
            if !self.get_current_char_or_panic().is_digit(10) {
                let ch = self.get_current_char_or_panic();
                panic!("Expected digit after float literal exponent, found {}", ch);
            }
            while self.get_current_char_or_panic().is_digit(10) {
                self.index += 1;
            }
        }

        let token_string_end = self.index;
        let float_string = &self.data[token_string_start..token_string_end];
        let val = float_string.parse().unwrap();
        return Some(Token { kind: Float(val), string: String::from(float_string) });
    }

    fn scan_int(&mut self) -> Option<Token> {
        let token_string_start = self.index;
        let mut base = 10;
        if let Some(ch) = self.get_current_char() {
            if ch == '0' {
                self.index += 1;
                if let Some(ch) = self.get_current_char() {
                    match ch.to_ascii_lowercase() {
                        'x' => {
                            self.index += 1;
                            base = 16;
                        },
                        'b' => {
                            self.index += 1;
                            base = 2;
                        },
                        'o' => {
                            self.index += 1;
                            base = 8;
                        },
                        _ => (),
                    };
                }
            }
        } else {
            return None;
        }
        let mut value = 0u64;
        // TODO: should syntax error if there is invalid character right after
        //       (including if digit is too big for the current base)
        while let Some(ch) = self.get_current_char() {
            if let Some(digit) = ch.to_digit(base) {
                // TODO: check for overflow
                value = value * base as u64 + digit as u64;
                self.index += 1;
            } else {
                break;
            }
        }

        let token_string_end = self.index;
        let string = String::from(&self.data[token_string_start..token_string_end]);
        return Some(Token { kind: Int(value), string });
    }

    fn get_single_or_double_char_operator(&mut self, single_op: TokenKind, double_op_char: char, double_op: TokenKind) -> TokenKind {
        if let Some(ch) = self.get_current_char() {
            if ch == double_op_char {
                self.index += 1;
                return double_op;
            }
        }
        return single_op;
    }

    fn scan_operator(&mut self) -> Option<Token> {
        let token_string_start = self.index;

        let ch = self.get_current_char()?;
        let kind = match ch {
            '<' => {
                self.index += 1;
                match self.get_current_char() {
                    Some('<') => {
                        self.index += 1;
                        match self.get_current_char() {
                            Some('=') => {
                                self.index += 1;
                                LShiftAssign
                            },
                            _ => LShift
                        }
                    },
                    Some('=') => {
                        self.index += 1;
                        LtEq
                    },
                    _ => Less
                }
            },
            '>' => {
                self.index += 1;
                match self.get_current_char() {
                    Some('>') => {
                        self.index += 1;
                        match self.get_current_char() {
                            Some('=') => {
                                self.index += 1;
                                RShiftAssign
                            },
                            _ => RShift
                        }
                    },
                    Some('=') => {
                        self.index += 1;
                        GtEq
                    },
                    _ => Greater
                }
            },
            '=' => { 
                self.index += 1;
                self.get_single_or_double_char_operator(Assign, '=', Equal)
            },
            '!' => {
                self.index += 1;
                self.get_single_or_double_char_operator(Not, '=', NotEq)
            },
            '&' => {
                self.index += 1;
                match self.get_current_char() {
                    Some('&') => {
                        self.index += 1;
                        match self.get_current_char() {
                            Some('=') => {
                                self.index += 1;
                                AndLogicAssign
                            },
                            _ => AndLogic,
                        }
                    },
                    Some('=') => {
                        self.index += 1;
                        AndBitAssign
                    },
                    _ => AndBit,
                }
            },
            '|' => {
                self.index += 1;
                match self.get_current_char() {
                    Some('|') => {
                        self.index += 1;
                        match self.get_current_char() {
                            Some('=') => {
                                self.index += 1;
                                OrLogicAssign
                            },
                            _ => OrLogic,
                        }
                    },
                    Some('=') => {
                        self.index += 1;
                        OrBitAssign
                    },
                    _ => OrBit,
                }
            },
            '+' => {
                self.index += 1;
                self.get_single_or_double_char_operator(Add, '=', AddAssign)
            },
            '-' => {
                self.index += 1;
                self.get_single_or_double_char_operator(Subtract, '=', SubAssign)
            },
            ':' => {
                self.index += 1;
                self.get_single_or_double_char_operator(Declare, '=', DeclareAssign)
            },
            '^' => {
                self.index += 1;
                self.get_single_or_double_char_operator(Xor, '=', XorAssign)
            },
            '*' => {
                self.index += 1;
                self.get_single_or_double_char_operator(Multiply, '=', MulAssign)
            },
            '/' => {
                self.index += 1;
                self.get_single_or_double_char_operator(Divide, '=', DivAssign)
            },
            '%' => {
                self.index += 1;
                self.get_single_or_double_char_operator(Modulus, '=', ModAssign)
            },
            '(' => {
                self.index += 1;
                OpenParen
            },
            ')' => {
                self.index += 1;
                CloseParen
            },
            '[' => {
                self.index += 1;
                OpenSquareBracket
            },
            ']' => {
                self.index += 1;
                CloseSquareBracket
            },
            '{' => {
                self.index += 1;
                OpenCurly
            },
            '}' => {
                self.index += 1;
                CloseCurly
            },
            ',' => {
                self.index += 1;
                Comma
            },
            ';' => {
                self.index += 1;
                SemiColon
            },

            // _ => return None,
            _ => panic!("Don't know how to handle token character '{}'", ch),
        };

        let token_string_end = self.index;
        let string = String::from(&self.data[token_string_start..token_string_end]);

        return Some(Token { kind, string });
    }

    fn next_token(&mut self) -> Option<Token> {
        let ch = self.get_current_char()?;

        if ch.is_whitespace() {
            while let Some(ch) = self.get_current_char() {
                if ch.is_whitespace() {
                    self.index += 1;
                } else {
                    return self.next_token();
                }
            }
            return None;
        } else if ch == '\'' {
            return self.scan_char();
        } else if ch == '"' {
            return self.scan_string();
        } else if ch.is_alphabetic() || ch == '_' {
            return self.scan_keyword_or_name();
        } else if ch == '.' {
            return self.scan_float();
        } else if ch.is_digit(10) {
            // check the first non digit character to see if
            // it's a . or e for floats
            let mut tmp = self.index;
            while let Some(ch) = self.get_char_at_index(tmp) {
                match ch {
                    '0' | '1' | '2' | '3' | '4' |
                        '5' | '6' | '7' | '8' | '9' => tmp += 1,
                        '.' | 'e' => return self.scan_float(),
                        _ => break,
                };
            };
            return self.scan_int();
        } else {
            return self.scan_operator();
        }

        // NOTE: remove warning suppression at top when this is removed
        panic!("Haven't handled all token cases");
        // self.index += 1;
        // Some(Token { kind: Str, string: &self.data[token_string_start..self.index] })
    }

    // fn parse_statement(&mut self) -> Statement {
    // }
}

struct ExprBinary<'a> {
    op: TokenKind,
    left: &'a Expression,
    right: &'a Expression
}

enum Expression {
    Int(u64),
    Float(f64),
    Str(String),
    Name(String),
}

struct BlockStatement {
    statements: Vec<Statement>,
}

struct ElseIfStatement {
    condition: Expression,
    block: BlockStatement,
}

struct IfStatement {
    condition: Expression,
    then_block: BlockStatement,
    elseifs: Vec<ElseIfStatement>,
    else_block: BlockStatement,
}

enum Statement {
    If(IfStatement),
    Block(BlockStatement),
    // Assign,
    Expression(Expression),
}

fn parse_expr_operand(stream: &mut Stream, token: Token) -> Expression {
    return match token.kind {
        Int(value) => {
            Expression::Int(value)
        },
        Float(value) => {
            Expression::Float(value)
        },
        Name(name) => {
            Expression::Name(name)
        },
        Str(string) => {
            Expression::Str(string)
        },
        _ => panic!("Unexpected token '{}' in expression", token.string),
    };
}

fn main() {
    let mut stream = Stream::from("
        if 1 == 2 {
        }
        foo : u64;
        foo := (bar: u64) u64 {
            return bar * 2;
        }
    ");

    // let mut names = Vec::new();

    println!("\n");
    // parse statement
    while let Some(token) = stream.next_token() {
        match token.kind {
            Name(name) => {
                let token = stream.next_token().unwrap();
                match token.kind {
                    Declare => {
                        let token = stream.next_token().unwrap();
                        match token.kind {
                            OpenParen => {
                                // function
                            },
                            Keyword(keyword) => {
                                // 
                            },
                            _ => panic!("Unexpected declaration type {}", token.string),
                        }
                    },
                    DeclareAssign => {
                    },
                    _ => panic!("Expected declaration, found {}", token.string),
                }
            },
            Keyword(keyword) => {
                fn parse_expression(stream: &mut stream) -> Expression {
                    loop {
                        let token = stream.next_token().unwrap();
                        match token.kind {
                            // Name(name) => {
                            //     let token = stream.next_token().unwrap();
                            //     // match token.kind {
                            //     // }
                            // },
                            Int(value) => {
                                end_value = value;
                            },
                            Equal => {
                                let value = parse_expression(stream);
                            },
                            SemiColon => break,
                            _ => panic!("invalid expression token: {}", token.string),
                        }
                    }
                }
                match keyword.as_str() {
                    "if" => {
                        // parse expression
                        let condition = parse_expression(&mut self);
                    },
                    _ => panic!("BAD"),
                }
            }
            _ => panic!("only declarations are allowed in global scope, found {}", token.string),
        }
    }
}

#[test]
fn lex_test() {
    let mut string = String::new();
    let tests = [
        ("foo", Token::new(Name("foo".to_string()), "foo")),
        ("12",    Token::new(Int(12), "12")),
        ("0X321", Token::new(Int(0x321), "0X321")),
        ("0B101", Token::new(Int(0b101), "0B101")),
        ("0O123", Token::new(Int(0o123), "0O123")),
        (".43",        Token::new(Float(0.43), ".43")),
        ("98.",        Token::new(Float(98.), "98.")),
        ("23.6435",    Token::new(Float(23.6435), "23.6435")),
        ("36.58297e2", Token::new(Float(36.58297e2), "36.58297e2")),
        ("23e3",       Token::new(Float(23e3), "23e3")),
        ("<",   Token::new(Less, "<")),
        (">",   Token::new(Greater, ">")),
        ("<<",  Token::new(LShift, "<<")),
        (">>",  Token::new(RShift, ">>")),
        (">>=", Token::new(RShiftAssign, ">>=")),
        ("<<=", Token::new(LShiftAssign, "<<=")),
        ("=",   Token::new(Assign, "=")),
        ("==",  Token::new(Equal, "==")),
        ("!",   Token::new(Not, "!")),
        ("!=",  Token::new(NotEq, "!=")),
        ("&",   Token::new(AndBit, "&")),
        ("&&",  Token::new(AndLogic, "&&")),
        ("&=",  Token::new(AndBitAssign, "&=")),
        ("&&=", Token::new(AndLogicAssign, "&&=")),
        ("|",   Token::new(OrBit, "|")),
        ("||",  Token::new(OrLogic, "||")),
        ("|=",  Token::new(OrBitAssign, "|=")),
        ("||=", Token::new(OrLogicAssign, "||=")),
        ("+",   Token::new(Add, "+")),
        ("+=",  Token::new(AddAssign, "+=")),
        ("-",   Token::new(Subtract, "-")),
        ("-=",  Token::new(SubAssign, "-=")),
        (":",   Token::new(Declare, ":")),
        (":=",  Token::new(DeclareAssign, ":=")),
        ("^",   Token::new(Xor, "^")),
        ("^=",  Token::new(XorAssign, "^=")),
        ("*",   Token::new(Multiply, "*")),
        ("*=",  Token::new(MulAssign, "*=")),
        ("/",   Token::new(Divide, "/")),
        ("/=",  Token::new(DivAssign, "/=")),
        ("%",   Token::new(Modulus, "%")),
        ("%=",  Token::new(ModAssign, "%=")),
        ("'a'", Token::new(Char('a'), "'a'")),
        ("'\\n'", Token::new(Char('\n'), "'\\n'")),
        ];
    for (token_string, _) in tests.iter() {
        string.push(' ');
        string.push_str(token_string);
    }
    let mut stream = Stream::from(&string);
    for (_, test_token) in tests.iter() {
        if let Some(token) = stream.next_token() {
            println!("{:?}, {:?}", token, test_token);
            assert_eq!(token, *test_token);
        } else {
            panic!("aaaa");
        }
    }

    let mut stream = Stream::from("foo+bar[] if (true) { foo := 0xf; } else { foo := 0o12; }");
    let tokens = [
        Token::new(Name("foo".to_string()), "foo"),
        Token::new(Add, "+"),
        Token::new(Name("bar".to_string()), "bar"),
        Token::new(OpenSquareBracket, "["),
        Token::new(CloseSquareBracket, "]"),
        Token::new(Keyword("if".to_string()), "if"),
        Token::new(OpenParen, "("),
        Token::new(Keyword("true".to_string()), "true"),
        Token::new(CloseParen, ")"),
        Token::new(OpenCurly, "{"),
        Token::new(Name("foo".to_string()), "foo"),
        Token::new(DeclareAssign, ":="),
        Token::new(Int(15), "0xf"),
        Token::new(SemiColon, ";"),
        Token::new(CloseCurly, "}"),
        Token::new(Keyword("else".to_string()), "else"),
        Token::new(OpenCurly, "{"),
        Token::new(Name("foo".to_string()), "foo"),
        Token::new(DeclareAssign, ":="),
        Token::new(Int(10), "0o12"),
        Token::new(SemiColon, ";"),
        Token::new(CloseCurly, "}"),
        ];

    for test_token in &tokens {
        let token = stream.next_token().unwrap();
        println!("{:?}", token);
        assert_eq!(token, *test_token);
    }
}
